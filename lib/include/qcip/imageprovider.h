#pragma once

#include <QtCore/QDir>
#include <QtCore/QSize>
#include <QtCore/QString>
#include <QtCore/QCryptographicHash>

#include <QtQuick/QQuickAsyncImageProvider>

#include <memory>
#include <chrono>

#include "qcip/global.h"

QCIP_BEGIN_NS

namespace detail {
    class NetworkImageProcessor;
}

class QCIP_API CachedImageProvider : public QQuickAsyncImageProvider
{

public:

    CachedImageProvider();
    CachedImageProvider(std::chrono::minutes cacheTime);
    CachedImageProvider(std::chrono::minutes cacheTime, QDir cacheDirectory);
    CachedImageProvider(std::chrono::minutes cacheTime, QDir cacheDirectory, QCryptographicHash::Algorithm hashAlgorithm);

    ~CachedImageProvider() override;

    QQuickImageResponse* requestImageResponse(const QString& id, const QSize& requestedSize) override;

    QDir const& cacheDirectory() const;
    void setCacheDirectory(QDir const& directory);

    std::chrono::duration<double> cacheTime() const;
    void setCacheTime(std::chrono::duration<double> time);

    QCryptographicHash::Algorithm hashAlgorithm() const;
    void setHashAlgorithm(QCryptographicHash::Algorithm algorithm);

private:

    QQuickImageResponse* createNetworkImageResponse(QUrl const& url, QString baseName, QSize requestedSize);

    QDir cacheDirectory_;
    QCryptographicHash::Algorithm hashAlgorithm_;
    std::chrono::duration<double> cacheTime_;

    std::unique_ptr<detail::NetworkImageProcessor> nip_;
};

QCIP_END_NS
