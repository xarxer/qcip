#pragma once

#include <QtCore/QEvent>

#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>

#include <unordered_map>

#include "qcip/global.h"
#include "networkimageevent.h"

QCIP_BEGIN_NS

namespace detail {

class QCIP_NOT_API NetworkImageProcessor : public QObject
{

    Q_OBJECT

public:

    NetworkImageProcessor();

    bool event(QEvent* event) override;

private slots:

    void onReplyReceived(QNetworkReply* reply);

private:

    std::unordered_map<QNetworkReply*, NetworkImageInfo::Ptr> responseMap_;

    QNetworkAccessManager nam_;

};

}

QCIP_END_NS
