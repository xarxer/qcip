#pragma once

#include <QtCore/QDir>
#include <QtCore/QUrl>
#include <QtCore/QEvent>
#include <QtCore/QString>

#include "qcip/global.h"
#include "networkimage.h"

QCIP_BEGIN_NS

class QCIP_NOT_API NetworkImageEvent : public QEvent
{

public:

    static const QEvent::Type Type = static_cast<QEvent::Type>(QEvent::Type::User + 1);

    NetworkImageEvent(NetworkImageInfo::Ptr info);

    NetworkImageInfo::Ptr imageInfo = nullptr;

};

QCIP_END_NS
