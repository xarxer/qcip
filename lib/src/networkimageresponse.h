#pragma once

#include <QtCore/QDir>
#include <QtCore/QString>
#include <QtQuick/QQuickImageResponse>

#include "qcip/global.h"

QCIP_BEGIN_NS

class QCIP_NOT_API NetworkImageResponse : public QQuickImageResponse
{

public:

    enum ErrorCode {
        None = 0,
        InvalidUrl,
        InvalidData,
        Timeout
    };

    NetworkImageResponse();

    void setImage(QImage image);
    void setError(ErrorCode code, QString const& errorString);

    QString errorString() const override;

    QQuickTextureFactory* textureFactory() const override;

private:

    ErrorCode errorCode_ = ErrorCode::None;
    QString errorString_ = "";

    QQuickTextureFactory* textureFactory_ = nullptr;
};

QCIP_END_NS
