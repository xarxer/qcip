#pragma once

#include <QtCore/QUrl>
#include <QtCore/QDir>
#include <QtCore/QSize>
#include <QtCore/QString>

#include "qcip/global.h"
#include "networkimageresponse.h"

QCIP_BEGIN_NS

struct QCIP_NOT_API NetworkImageInfo {

    using Ptr = std::unique_ptr<NetworkImageInfo>;

    NetworkImageInfo(QUrl url, QDir saveLocation, QString baseName, QSize requestedSize, NetworkImageResponse* response) :
        url(std::move(url)), saveLocation(std::move(saveLocation)), baseName(std::move(baseName)), requestedSize(requestedSize), response(response)
    { }

    QUrl    url;
    QDir    saveLocation;
    QString baseName;
    QSize   requestedSize;

    NetworkImageResponse* response = nullptr;
};

QCIP_END_NS
