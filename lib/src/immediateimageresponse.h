#pragma once

#include <QtQuick/QQuickImageResponse>
#include <QtQuick/QQuickTextureFactory>

#include "qcip/global.h"

QCIP_BEGIN_NS

class QCIP_NOT_API ImmediateImageResponse : public QQuickImageResponse
{

    Q_OBJECT

public:

    ImmediateImageResponse(QString const& fileName, QSize const& requestedSize);

    QQuickTextureFactory* textureFactory() const override;

private:

    QQuickTextureFactory* textureFactory_ = nullptr;

};

QCIP_END_NS
