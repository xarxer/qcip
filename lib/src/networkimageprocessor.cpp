#include "networkimageprocessor.h"

#include <QtGui/QImage>
#include <QtGui/QImageWriter>

#include <QtCore/QMimeDatabase>

QCIP_BEGIN_NS

namespace detail {

NetworkImageProcessor::NetworkImageProcessor()
{
    QObject::connect(&nam_, &QNetworkAccessManager::finished, this, &NetworkImageProcessor::onReplyReceived);
}

bool NetworkImageProcessor::event(QEvent* event)
{
    if(event->type() == NetworkImageEvent::Type) {
        NetworkImageEvent* e = static_cast<NetworkImageEvent*>(event);

        // Sanity check
        if(e->imageInfo->url.isValid() == false) {
            e->imageInfo->response->setError(NetworkImageResponse::InvalidUrl,
                                             QString("%1: %2").arg("Invalid url for image:", e->imageInfo->url.toString()));
        } else {
            QNetworkReply* reply = nam_.get(QNetworkRequest(e->imageInfo->url));
            responseMap_.try_emplace(reply, std::move(e->imageInfo));
        }

        event->accept();
        return true;
    }

    return QObject::event(event);
}

void NetworkImageProcessor::onReplyReceived(QNetworkReply* reply)
{
    NetworkImageInfo::Ptr imageInfo = std::move(responseMap_[reply]);
    QByteArray imageData = reply->readAll();

    reply->deleteLater();

    // Sanity check
    if(imageData.size() == 0) {
        imageInfo->response->setError(NetworkImageResponse::InvalidData, "Invalid data read from resource");
    } else {
        qDebug() << imageData.size() << "bytes fetched";

        QImage image = QImage::fromData(imageData);
        if(imageInfo->requestedSize.isValid()) {
            image = image.scaled(imageInfo->requestedSize);
        }

        {
            QString fileSuffix = QMimeDatabase().mimeTypeForData(imageData).preferredSuffix();
            QString fileName   = QString("%1.%2").arg(imageInfo->baseName, fileSuffix);

            QImageWriter writer(QString(imageInfo->saveLocation.absoluteFilePath(fileName)));
            writer.write(image);
        }

        imageInfo->response->setImage(image);
    }
}

}

QCIP_END_NS
