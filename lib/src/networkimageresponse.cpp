#include "networkimageresponse.h"

#include <QtGui/QImageWriter>
#include <QtCore/QMimeDatabase>

QCIP_BEGIN_NS

NetworkImageResponse::NetworkImageResponse()
{ }

void NetworkImageResponse::setImage(QImage image)
{
    textureFactory_ = QQuickTextureFactory::textureFactoryForImage(image);
    emit finished();
}

void NetworkImageResponse::setError(NetworkImageResponse::ErrorCode code, const QString& errorString)
{
    errorCode_ = code;
    errorString_ = errorString;

    if(errorCode_ != ErrorCode::None) {
        emit finished();
    }
}

QString NetworkImageResponse::errorString() const
{
    return errorString_;
}

QQuickTextureFactory* NetworkImageResponse::textureFactory() const
{
    return textureFactory_;
}

QCIP_END_NS
