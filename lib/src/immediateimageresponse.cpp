#include "immediateimageresponse.h"

#include <QtCore/QTimer>
#include <QtGui/QImage>

QCIP_BEGIN_NS

ImmediateImageResponse::ImmediateImageResponse(QString const& fileName, QSize const& requestedSize)
{
    QImage image(fileName);

    if(requestedSize.isValid()) {
        image = image.scaled(requestedSize);
    }

    textureFactory_ = QQuickTextureFactory::textureFactoryForImage(image);

    // Trigger finished on the next event processing cycle
    QTimer::singleShot(0, [this]() {
        emit finished();
    });
}

QQuickTextureFactory* ImmediateImageResponse::textureFactory() const
{
    return textureFactory_;
}

QCIP_END_NS
