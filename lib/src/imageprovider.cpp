#include "qcip/imageprovider.h"

#include <QtCore/QFileInfo>
#include <QtCore/QDirIterator>
#include <QtCore/QStandardPaths>
#include <QtCore/QCoreApplication>

#include <optional>

#include "immediateimageresponse.h"
#include "networkimageresponse.h"
#include "networkimageprocessor.h"

static std::chrono::hours DefaultCacheTime = std::chrono::hours(24);
static QDir DefaultCacheDirectory = QDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
static QCryptographicHash::Algorithm DefaultHashAlgorithm = QCryptographicHash::Sha1;

static QString createHash(QString const& data, QCryptographicHash::Algorithm algo) {
    return QCryptographicHash::hash(data.toUtf8(), algo).toHex();
}

static std::optional<QFileInfo> fileInfoFromBaseName(QString const& baseName, QDir const& directory)
{
    QDirIterator it(directory.absolutePath(), QDir::Files | QDir::NoDotAndDotDot);
    while(it.hasNext()) {
        QFileInfo fi(it.next());

        if(fi.baseName() == baseName) {
            return std::move(fi);
        }
    }

    return std::nullopt;
}

static bool hasTimedOut(QDateTime const& cache, std::chrono::duration<double> storageTime) {
    auto storageMillis = std::chrono::duration_cast<std::chrono::milliseconds>(storageTime).count();
    return cache.msecsTo(QDateTime::currentDateTime()) > storageMillis;
}

QCIP_BEGIN_NS

CachedImageProvider::CachedImageProvider() :
    CachedImageProvider(DefaultCacheTime, DefaultCacheDirectory, DefaultHashAlgorithm)
{ }

CachedImageProvider::CachedImageProvider(std::chrono::minutes cacheTime) :
    CachedImageProvider(cacheTime, DefaultCacheDirectory, DefaultHashAlgorithm)
{ }

CachedImageProvider::CachedImageProvider(std::chrono::minutes cacheTime, QDir cacheDirectory) :
    CachedImageProvider(cacheTime, cacheDirectory, DefaultHashAlgorithm)
{ }

CachedImageProvider::CachedImageProvider(std::chrono::minutes cacheTime, QDir cacheDirectory, QCryptographicHash::Algorithm hashAlgorithm) :
    cacheDirectory_(cacheDirectory),
    hashAlgorithm_(hashAlgorithm),
    cacheTime_(cacheTime),
    nip_(std::make_unique<detail::NetworkImageProcessor>())
{ }

CachedImageProvider::~CachedImageProvider()
{ }

QQuickImageResponse* CachedImageProvider::requestImageResponse(const QString& id, const QSize& requestedSize)
{
    // hashValue is the base name of the cached file
    QString hashValue = createHash(id, hashAlgorithm_);
    auto opt_fi = fileInfoFromBaseName(hashValue, cacheDirectory_);

    if(opt_fi) {
        if(hasTimedOut(opt_fi->lastModified(), cacheTime_)) {
            qDebug() << "Image with URL" << id << "found in cache, but is too old, fetching from network.";
        } else {
            qDebug() << "Image with URL" << id << "found in cache.";
            return new ImmediateImageResponse(opt_fi->absoluteFilePath(), requestedSize);
        }
    } else {
        qDebug() << "Image with URL" << id << "not found in cache, fetching from network.";
    }


    return createNetworkImageResponse(QUrl(id), hashValue, requestedSize);
}

QDir const& CachedImageProvider::cacheDirectory() const
{
    return cacheDirectory_;
}

void CachedImageProvider::setCacheDirectory(QDir const& directory)
{
    cacheDirectory_ = directory;
}

std::chrono::duration<double> CachedImageProvider::cacheTime() const
{
    return cacheTime_;
}

void CachedImageProvider::setCacheTime(std::chrono::duration<double> time)
{
    cacheTime_ = time;
}

QCryptographicHash::Algorithm CachedImageProvider::hashAlgorithm() const
{
    return hashAlgorithm_;
}

void CachedImageProvider::setHashAlgorithm(QCryptographicHash::Algorithm algorithm)
{
    hashAlgorithm_ = algorithm;
}

QQuickImageResponse* CachedImageProvider::createNetworkImageResponse(QUrl const& url, QString baseName, QSize requestedSize)
{
    NetworkImageResponse* response = new NetworkImageResponse;
    QCoreApplication::postEvent(nip_.get(),
                                new NetworkImageEvent(
                                    std::make_unique<NetworkImageInfo>(
                                        url, cacheDirectory_, baseName, requestedSize, response)));

    return response;
}

QCIP_END_NS
