#include "networkimageevent.h"

QCIP_BEGIN_NS

NetworkImageEvent::NetworkImageEvent(NetworkImageInfo::Ptr info) :
    QEvent(Type),
    imageInfo(std::move(info))
{ }

QCIP_END_NS
