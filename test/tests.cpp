#include <QtTest/QTest>

#include <QtCore/QThread>
#include <QtCore/QEventLoop>
#include <QtCore/QDirIterator>
#include <QtCore/QTemporaryDir>

#include <vector>

#include <qcip/imageprovider.h>

class QCIPTests : public QObject {

    Q_OBJECT

public:

    QCIPTests() {
        provider_.setCacheDirectory(tempDir_.path());
    }

private slots:

    void init() {
        // Reset cache time in case any TC alters it
        provider_.setCacheTime(std::chrono::hours(24));

        // Delete all files from the temp directory
        QDir dir(tempDir_.path());
        QDirIterator it(tempDir_.path(), QDir::Files | QDir::NoDotAndDotDot);

        while(it.hasNext()) {
            QFile file(it.next());
            if(dir.remove(it.fileName()) == false) {
                qWarning() << "Failed to delete file";
            }
        }
    }

    void imagesCanBeDownloaded() {
        UrlHashPair const& subject = images[0];

        QVERIFY(!fileWithBaseNameExists(subject.hash));

        QQuickImageResponse* response = provider_.requestImageResponse(subject.url, QSize());

        QEventLoop loop;
        QObject::connect(response, &QQuickImageResponse::finished, &loop, &QEventLoop::quit);
        loop.exec();

        QVERIFY(fileWithBaseNameExists(subject.hash));
    }

    void downloadedImagesWillNotDownloadAgain() {
        QQuickImageResponse* response = nullptr;
        UrlHashPair const& subject = images[1];

        QDateTime modifyTime1;
        QDateTime modifyTime2;

        QVERIFY(!fileWithBaseNameExists(subject.hash));

        {   // Download the image
            response = provider_.requestImageResponse(subject.url, QSize());

            QEventLoop loop;
            QObject::connect(response, &QQuickImageResponse::finished, &loop, &QEventLoop::quit);
            loop.exec();

            QFileInfo fi = fileFromBaseName(subject.hash);
            modifyTime1 = fi.lastModified();
        }

        {   // Request same image again
            response = provider_.requestImageResponse(subject.url, QSize());

            QEventLoop loop;
            QObject::connect(response, &QQuickImageResponse::finished, &loop, &QEventLoop::quit);
            loop.exec();

            QFileInfo fi = fileFromBaseName(subject.hash);
            modifyTime2 = fi.lastModified();
        }

        QVERIFY(modifyTime1 == modifyTime2);
    }

    void canDownloadInParallell() {
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using clock_t = std::chrono::steady_clock;

        QQuickImageResponse* respone1 = nullptr;
        QQuickImageResponse* respone2 = nullptr;
        QQuickImageResponse* respone3 = nullptr;

        bool done1 = false;
        bool done2 = false;
        bool done3 = false;

        respone1 = provider_.requestImageResponse(images[0].url, QSize());
        respone2 = provider_.requestImageResponse(images[1].url, QSize());
        respone3 = provider_.requestImageResponse(images[2].url, QSize());

        QObject::connect(respone1, &QQuickImageResponse::finished, [&done1]() { done1 = true; });
        QObject::connect(respone1, &QQuickImageResponse::finished, [&done1]() { done1 = true; });
        QObject::connect(respone1, &QQuickImageResponse::finished, [&done1]() { done1 = true; });

        QEventLoop loop;

        clock_t::time_point start = clock_t::now();
        while(!done1 && !done2 && !done3) {
            loop.processEvents();

            auto elapsed = duration_cast<milliseconds>(start - clock_t::now());
            if(elapsed.count() >= 10'000) {
                QFAIL("Image download exceeded 10 000 milliseconds");
            }
        }

        QVERIFY(respone1->errorString().isEmpty());
        QVERIFY(respone2->errorString().isEmpty());
        QVERIFY(respone3->errorString().isEmpty());
    }

    void invalidatedImagesAreDownloadedAgain() {
        provider_.setCacheTime(std::chrono::milliseconds(1));
        UrlHashPair const& subject = images[0];

        QDateTime modifyTime1;
        QDateTime modifyTime2;

        {   // Download file
            QVERIFY(!fileWithBaseNameExists(subject.hash));

            QQuickImageResponse* response = provider_.requestImageResponse(subject.url, QSize());

            QEventLoop loop;
            QObject::connect(response, &QQuickImageResponse::finished, &loop, &QEventLoop::quit);
            loop.exec();

            QFileInfo fi = fileFromBaseName(subject.hash);
            QVERIFY(fi.exists());

            modifyTime1 = fi.lastModified();
        }

        QThread::msleep(5);

        {   // Download file
            QVERIFY(fileWithBaseNameExists(subject.hash)); // It's already downloaded, but should be invalid

            QQuickImageResponse* response = provider_.requestImageResponse(subject.url, QSize());

            QEventLoop loop;
            QObject::connect(response, &QQuickImageResponse::finished, &loop, &QEventLoop::quit);
            loop.exec();

            QFileInfo fi = fileFromBaseName(subject.hash);
            QVERIFY(fi.exists());

            modifyTime2 = fi.lastModified();
        }

        QVERIFY(modifyTime1 != modifyTime2);
    }

private:

    struct UrlHashPair {
        QString url;
        QString hash;
    };

    bool fileWithBaseNameExists(QString baseName) {
        QDirIterator it(tempDir_.path(), QDir::Files | QDir::NoDotAndDotDot);
        while(it.hasNext()) {
            QFileInfo fi(it.next());

            if(fi.baseName() == baseName) {
                return true;
            }
        }

        return false;
    }

    QFileInfo fileFromBaseName(QString baseName) {
        QDirIterator it(tempDir_.path(), QDir::Files | QDir::NoDotAndDotDot);
        while(it.hasNext()) {
            QFileInfo fi(it.next());

            if(fi.baseName() == baseName) {
                return fi;
            }
        }

        return QFileInfo();
    }

    QString createHash(QString const& data, QCryptographicHash::Algorithm algorithm) {
        return QCryptographicHash::hash(data.toUtf8(), algorithm).toHex();
    }

    qcip::CachedImageProvider provider_;
    QTemporaryDir tempDir_;

    std::vector<UrlHashPair> images {
        { "https://via.placeholder.com/10x10", createHash("https://via.placeholder.com/10x10", provider_.hashAlgorithm()) },
        { "https://via.placeholder.com/20x10", createHash("https://via.placeholder.com/20x10", provider_.hashAlgorithm()) },
        { "https://via.placeholder.com/30x10", createHash("https://via.placeholder.com/30x10", provider_.hashAlgorithm()) },
        { "https://via.placeholder.com/40x10", createHash("https://via.placeholder.com/40x10", provider_.hashAlgorithm()) },
    };

};

QTEST_MAIN(QCIPTests)
#include "tests.moc"
